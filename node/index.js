var io = require('socket.io')(6001, {
    cors: {
        origin: "*"
    }
});
var Redis = require('ioredis');
var redis = new Redis({ host: 'redis' });

redis.psubscribe('*', function(error, count) {

});

redis.on('pmessage', function(pattern, channel, message) {
    console.log(message);
    message = JSON.parse(message);
    io.emit(channel + ':' + message.data.message.chat_id, message.data.message);
});
