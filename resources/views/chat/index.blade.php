@extends('layouts.master')


@section('content')
    <h2>Чат с {{$friend_name}}</h2>
    <a href="{{ route('site.index') }}">назад</a>
    <div class="bg-light p-5 rounded">
        <ul class="chat">
            @foreach($messages as $message)
                <li>
                    <b>{{ $message->author_name }}:</b>
                    <p>{{ $message->content }}</p>
                </li>
            @endforeach
        </ul>
        <hr>
        <form id="message_form" method="POST">
            @csrf
            <input type="hidden" name="chat_id" value="{{ $chat_id }}" />
            <input type="hidden" name="author_id" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" />
            <textarea name="content" id="message_content" style="width: 100%; height: 50px"></textarea>
            <input type="submit" id="submit" value="Отправить">
        </form>
    </div>

    <script src="https://cdn.socket.io/4.5.0/socket.io.min.js" integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            let socket = io(':6001');

            function appendMessage(data) {
                $('.chat').append(
                    $('<li/>').append(
                        $('<b/>').text(data.author_name+":"),
                        $('<p/>').text(data.content),
                        //data.message
                    )
                );
            }

            function appendMessageLocal(content, indicator) {
                $('.chat').append(
                    $('<li/>').append(
                        $('<b/>').text('{{\Illuminate\Support\Facades\Auth::user()->name}}:'),
                        $('<p/>').text(content).append(
                            $('<span data-indicator="'+ indicator +'"/>').text(' (...)')
                        ),
                    )
                );
            }

            socket.on('chat:{{$chat_id}}', function (data) {
                if (data.author_id !== {{\Illuminate\Support\Facades\Auth::user()->id}}) {
                    appendMessage(data);
                }
            });

            var form = $("#message_form");
            var submit = $("#submit");
            var textbox = $("#message_content");

            submit.bind('click', function(event) {
                var content = textbox.val();
                event.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var indicator = $('.chat li').length + 1;
                appendMessageLocal(content, indicator);
                $.ajax({
                    url: "{{ route('message.send') }}",
                    type: "POST",
                    data: form.serialize(),
                    success: function() {
                        $('*[data-indicator='+ indicator +']').remove();
                    },
                    error: function() {
                        $('*[data-indicator='+ indicator +']').text(' (сообщение не отправлено)');
                    }
                });
                textbox.val('');
            });
        });
    </script>
@endsection
